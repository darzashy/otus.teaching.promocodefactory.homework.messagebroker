﻿using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.Integration.EventHandlers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.Integration
{
    public class AdministrationEventsConsumer : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IAdminEventHandler _eventHandler;
        private IConnection _connection;
        private IModel _channel;
        private const string QueueNotifyAdmin = "NotifyAdminAboutPartnerManagerPromoCode";

        public AdministrationEventsConsumer(
            IConfiguration configuration,
            IAdminEventHandler eventHandler)
        {
            _configuration = configuration;
            _eventHandler = eventHandler;
            Initialize();
        }

        private void Initialize()
        {
            var factory = new ConnectionFactory()
            {
                UserName = "rmuser",
                Password = "rmpassword",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(exchange: QueueNotifyAdmin, type: ExchangeType.Fanout);

            _channel.QueueDeclare(queue: QueueNotifyAdmin,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            _channel.QueueBind(queue: QueueNotifyAdmin,
                               exchange: QueueNotifyAdmin,
                               routingKey: "");
        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }

            base.Dispose();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, args) =>
            {
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());

                _eventHandler.UpdatePromoCodesEvent(message);
            };

            _channel.BasicConsume(queue: QueueNotifyAdmin,
                                    autoAck: true, consumer);

            return Task.CompletedTask;
        }
    }
}