﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.EventHandlers
{
    public interface IGivingEventHandler
    {
        Task GivePromoCodeToCustomer(string message);
    }
}