﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.EventHandlers
{
    public class GivingEventHandler : IGivingEventHandler
    {
        private readonly IServiceScopeFactory _scopeFactory;
        
        public GivingEventHandler(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task GivePromoCodeToCustomer(string notificationMessage)
        {
            using var scope = _scopeFactory.CreateScope();
            var customersRepository = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();
            var preferencesRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
            var promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>(); 
            var promoCodeNotified = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(notificationMessage);
                
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<GivingEventHandler>>();

            try
            {
                //Получаем предпочтение по имени
                var preference = await preferencesRepository.GetByIdAsync(promoCodeNotified.PreferenceId);

                if (preference == null)
                    return;

                //  Получаем клиентов с этим предпочтением:
                var customers = await customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id));

                var promoCode = new PromoCode
                {
                    Id = promoCodeNotified.PromoCodeId,
                    PartnerId = promoCodeNotified.PartnerId,
                    Code = promoCodeNotified.PromoCode,
                    ServiceInfo = promoCodeNotified.ServiceInfo,
                    BeginDate = DateTime.Parse(promoCodeNotified.BeginDate),
                    EndDate = DateTime.Parse(promoCodeNotified.EndDate),
                    Preference = preference,
                    PreferenceId = preference.Id,
                    Customers = new List<PromoCodeCustomer>()
                };

                foreach (var item in customers)
                {
                    promoCode.Customers.Add(new PromoCodeCustomer
                    {

                        CustomerId = item.Id,
                        Customer = item,
                        PromoCodeId = promoCode.Id,
                        PromoCode = promoCode
                    });
                };

                await promoCodesRepository.AddAsync(promoCode);

            }
            catch (Exception e)
            {
                logger.LogError($"Ошибка при добавлении промокодов пользователям: {e.Message}");
                throw;
            }
        }
    }
}