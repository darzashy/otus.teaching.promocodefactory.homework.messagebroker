﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models
{
    public class NotifyAdminAboutPartnerManagerPromoCodeDto
    {
        public Guid? PartnerManagerId { get; set; }
    }
}